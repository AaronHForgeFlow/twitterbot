#!/usr/bin/env python

import tweepy


from shutil import copyfile


def store_last_id(tweet_id, word="tormenta"):
    """ Store a tweet id in a file for specific word"""
    filename="lastid"
    copyfile(filename, "bk_file")
    #input file
    fin = open("bk_file", "r")
    #output file to write the result to
    fout = open(filename, "w")
    #for each line in the input file
    seen = False
    for line in fin.readlines():
        #read replace the string and write to output file
        wd = line.split(':')[0]
        if word == wd:
            new_line = wd + ':' + str(tweet_id)
            seen = True
        else:
            new_line = line
        fout.write(new_line)
    if not seen:
        new_line = word + ':' + str(tweet_id)
        fout.write(new_line)
    #  start in new line
    fout.write('\n')
    #close input and output files
    fin.close()
    fout.close()


def get_last_id(word="tormenta"):
    """ Read the last retweeted id from a file for the word"""
    filename="lastid"
    with open(filename, "r") as fp:
        for line in fp.readlines():
            wd = line.split(':')[0]
            if word == wd:
                return int(line.split(':')[1])
    return 999


def post_tweet(cr, tweet, media=False):
    # publish a tweet
    if not media:
        cr.update_status(tweet)
    else:
        cr.update_with_media(media, status=tweet)


def connect():
    with open('keys.txt') as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    consumer_key = content[0]
    consumer_secret = content[1]
    access_token = content[2]
    access_token_secret = content[3]
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    cr = tweepy.API(auth)
    return cr


if __name__ == '__main__':
    cr = connect()
