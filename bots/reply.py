#!/usr/bin/env python
# tweepy-bots/bots/autoreply.py

import tweepy
import logging
import sys
from lib import connect, get_last_id, store_last_id
sys.path.append("..")

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()


def check_mentions(cr, keywords, since_id):
    logger.info("Retrieving mentions")
    new_since_id = since_id
    for tweet in tweepy.Cursor(cr.mentions_timeline,
        since_id=since_id).items():
        new_since_id = max(tweet.id, new_since_id)
        if tweet.in_reply_to_status_id is not None:
            continue
        if any(keyword in tweet.text.lower() for keyword in keywords):
            logger.info(f"Answering to {tweet.user.name}")

            if not tweet.user.following:
                tweet.user.follow()

            cr.update_status(
                status="Thank you!",
                in_reply_to_status_id=tweet.id,
            )
            store_last_id(tweet.id, 'lastreplyid')
    return new_since_id


def main():
    cr = connect()
    since_id = get_last_id('lastreplyid')
    # while True:
    check_mentions(cr, [""], since_id)
    logger.info("Waiting...")
    # time.sleep(3660)


if __name__ == "__main__":
    main()
