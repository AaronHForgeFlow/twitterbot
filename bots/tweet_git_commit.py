# -*- coding: utf-8 -*-
import sys
sys.path.append("..")
from lib import connect, post_tweet
import logging
logger = logging.getLogger(__name__)
import random
VALID_CODES = ['TWC']


def get_module_summary(path, module):
    path = path+module+'/__manifest__.py'
    descr = ""
    with open(path) as f1:
        n_lines = f1.readlines()
    for line in n_lines:
        if "'summary':" in line or '"summary":' in line:
            descr = line.split("summary", 1)[1]
            descr = descr[2:-1]
    return descr + '. '


def get_module_name(path, module):
    path = path+module+'/__manifest__.py'
    descr = ""
    with open(path) as f1:
        n_lines = f1.readlines()
    for line in n_lines:
        if "'name':" in line or '"name":' in line:
            descr = line.split("name", 1)[1]
            return descr[2:-1]
    return descr


def main_message(message, website):
    cool_words = ["Crazy: ", "Awesome: ", "Amazing: ", "Pretty nice: ",
                  "Don't miss: "]
    word = random.choice(cool_words)
    return word + message + \
            website + \
            ' @ForgeFlow'        


def get_icon(path, module, icon_path):
    if icon_path:
        return icon_path
    return path + module + '/static/description/icon.png'


def apply_format(tweet):
    # remove stupid quotes
    tweet = tweet.replace('"','')
    tweet = tweet.replace("'",'')
    return tweet


if __name__ == '__main__':
    path = ""
    module = ""
    m_type = input('"readme" for descripton, or custom text for custom message: ')
    icon_path = input('Path to custom picture (empty for no icon): ')
    if len(icon_path) < 3:
        icon_path = '/home/odoo/Pictures/forgeflow-logo-no-letters.png'
    website = input('Custom website: ')
    if m_type == 'readme':
        path = input('path to repo: ')
        module = input('module: ')
        message = get_module_summary(
            path=path, module=module)
    else:
        message = m_type
    tweet = main_message(message, website)
    tweet = apply_format(tweet) 
    icon = get_icon(path, module, icon_path)
    if not tweet:
        print('Commit without tweet code')
    else:
        print(tweet)
        while True:
            test = input('Do you want to tweet? (y/n)')
            if test == 'y':
                cr = connect()
                if icon:
                    post_tweet(cr, tweet, icon)
                else:
                    post_tweet(cr, tweet)
                break
            elif test == 'n':
                break
            else:
                print('Please type y(yes) or n(no)')
