#!/usr/bin/env python

import logging
from lib import connect, get_last_id, store_last_id
import sys
from tweepy.error import TweepError
sys.path.append("..")


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()


def retweet(cr, keywords):
    for tweet in cr.home_timeline(count=100):
        for word in keywords:
            if word in tweet.text:
                tweet.retweet()
                print("%s was retweeted" % tweet.text)
                return tweet.id

def favourite(cr, keywords):
    for tweet in cr.home_timeline(count=100):
        for word in keywords:
            if word in tweet.text:
                try:
                    cr.create_favorite(tweet.id)
                except TweepError:
                    print(tweet.text + ' already favourited!')
                    break
                print("%s was favourited" % tweet.text)
                return tweet.id

def favourite_and_retweet(cr, keywords):
    for tweet in cr.home_timeline(count=100):
        for word in keywords:
            if word in tweet.text:
                while True:
                    print(tweet.text)
                    do_post = input('do you want to tweet? y(yes),n(no) ')
                    if do_post == 'y':
                        cr.create_favorite(tweet.id)
                        tweet.retweet()
                        print("%s was favourited and rt" % tweet.text)
                        return tweet.id
                    elif do_post == 'n':
                        break
                    else:
                        print('please answer y or n')


def unfavourite(cr, last_id):
    cr.destroy_favorite(last_id)
    print("%s was unfavourited" % last_id)


def main(keywords, rt):
    cr = connect()
    if rt == 'fav':
        favourite(cr, keywords)
    elif rt == 'fav_ret':
        favourite_and_retweet(cr, keywords)
    elif rt == 'ret':
        retweet(cr, keywords)
    elif rt == 'unfav':
        lastid = input('tell me the id to unfav')
        unfavourite(cr, lastid)


if __name__ == "__main__":
    largs = len(sys.argv)
    print(sys.argv)
    if largs < 2:
        keyword = 'ForgeFlow'
    else:
        keyword = sys.argv[1]
    if largs < 3:
        rt = 'fav'
    else:
        rt = sys.argv[2]
    main([keyword], rt)
