#!/usr/bin/env python
# tweepy-bots/bots/followfollowers.py

import tweepy
import logging
import sys
sys.path.append("..")
from lib import connect
import time

logger = logging.getLogger()


def follow_followers(cr):
    logger.info("Retrieving and following followers")
    for follower in tweepy.Cursor(cr.followers).items():
        if not follower.following:
            logger.info(f"Following {follower.name}")
            follower.follow()


def main():
    cr = connect()
    while True:
        follow_followers(cr)
        logger.info("Waiting...")
        time.sleep(3600)


if __name__ == "__main__":
    main()
