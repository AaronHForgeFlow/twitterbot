# twitterbot git
# License AGPL-3.0
# Author: Aaron Henriquez

A twitter bot that publish your git contributions and more

## Depends on
tweepy>=2.1
requests
beautifulsoup4

### tweet git contribution example
python bots/tweet_git_commit.py folder branch module** commit_number** option* icon_path**

*option: commit,readme,custom_message
** optional parameters

### useful for atomation

# bash to retweet
. twitterbot/venv/bin/activate
cd twitterbot/bots/
python retweet.py > /home/odoo/backups/logs/cron.log 2>&1 &

# bash to stop
kill -9 `ps -ef | grep retweet.py | grep -v grep | awk '{print $2}'`

# crontab

00 */4 * * * /home/odoo/tweet-run.sh > /home/odoo/backups/logs/cron.log 2>> /home/odoo/backups/logs/cron_error.log
